const [...list] = document.getElementsByTagName('ul');
const [...items] = document.querySelectorAll('#list li');

const one = document.querySelector('.one');
const two = document.querySelector('.two');
const three = document.querySelector('.three');
const four = document.querySelector('.four');
const five = document.querySelector('.five');
const six = document.querySelector('.six');
const seven = document.querySelector('.seven');

list.forEach(element => {

    one.onclick = () => {
        for (let elem of items) {
            if (elem.className === 'active') {
                elem.removeAttribute('class');
            } element.firstElementChild.setAttribute('class', 'active');
        }
    }
    two.onclick = () => {
        for (let elem of items) {
            if (elem.className === 'active') {
                elem.removeAttribute('class');
            } element.lastElementChild.setAttribute('class', 'active');
        }
    }
    three.onclick = () => {
        for (let elem of items) {
            if (elem.className === 'active') {
                elem.nextElementSibling.setAttribute('class', 'active');
                elem.removeAttribute('class');
                return;
            } else if (element.lastElementChild.className === 'active') {
                element.firstElementChild.setAttribute('class', 'active')
                element.lastElementChild.removeAttribute('class');
            }
        }
    }
    four.onclick = () => {
        for (let elem of items) {
            if (element.firstElementChild.className === 'active') {
                element.lastElementChild.setAttribute('class', 'active')
                element.firstElementChild.removeAttribute('class');
                return;
            } else if (elem.className === 'active') {
                elem.removeAttribute('class');
                elem.previousElementSibling.setAttribute('class', 'active');
                return;
            }
        }
    }
    
    five.onclick = () => {
        let counter = items.length;
        let liAdd = document.createElement('li');
        liAdd.setAttribute('class', 'li');
        liAdd.innerText = `List Item ${++counter}`;

        for (let elem of list) {
            items.push(elem.appendChild(liAdd));
        }
    }
    for (let elem of list) {
        six.onclick = () => {
            items.pop(elem.lastElementChild.remove());
        }
    }
    seven.onclick = () => {
        
        let counter = items.length;
        let liPredend = document.createElement('li');
        liPredend.innerText = `List Item ${++counter}`;
        liPredend.setAttribute('class', 'li');
        items.unshift(liPredend);
        element.prepend(liPredend);

       
    }
});

